package com.boldcolour.boldcolourbackend.service;

import com.boldcolour.boldcolourbackend.model.dto.ShopAdminDto;
import com.boldcolour.boldcolourbackend.model.mapper.ShopAdminMapper;
import com.boldcolour.boldcolourbackend.model.repository.ShopAdminRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShopAdminService {
    private final ShopAdminMapper shopAdminMapper;
    private final ShopAdminRepository shopAdminRepository;

    public ShopAdminDto addShopAdmin(final ShopAdminDto shopAdminDto) {
        var optionalShopAdmin = shopAdminRepository.findShopAdminByCellphone(shopAdminDto.getCellphone());
        if (optionalShopAdmin.isEmpty()) {
            var shopAdmin = shopAdminMapper.toEntity(shopAdminDto);
            return shopAdminMapper.toDto(shopAdminRepository.save(shopAdmin));
        } else {
            throw new EntityExistsException("An admin with cellphone " + shopAdminDto.getCellphone() + " already exists.");
        }
    }

    public void deleteShopAdmin(final Long id) {
        var optionalShopAdmin = shopAdminRepository.findById(id);
        if (optionalShopAdmin.isPresent()) {
            shopAdminRepository.delete(optionalShopAdmin.get());
        } else {
            throw new EntityNotFoundException("Shop admin with ID " + id + " does not exist.");
        }
    }

    public ShopAdminDto updateShopAdmin(final Long id, final ShopAdminDto shopAdminDto) {
        var optionalShopAdmin = shopAdminRepository.findById(id);
        if (optionalShopAdmin.isPresent()) {
            var shopAdmin = optionalShopAdmin.get();
            shopAdmin.setCellphone(shopAdminDto.getCellphone());
            shopAdmin.setFcmToken(shopAdminDto.getFcmToken());
            shopAdmin.setFirebaseUid(shopAdminDto.getFirebaseUid());
            shopAdmin.setFirstName(shopAdminDto.getFirstName());
            shopAdmin.setEmail(shopAdminDto.getEmail());
            shopAdmin.setLastName(shopAdminDto.getLastName());
            shopAdmin.setPhotoUrl(shopAdminDto.getPhotoUrl());
            return shopAdminMapper.toDto(shopAdminRepository.save(shopAdmin));
        } else {
            throw new EntityNotFoundException("Shop admin with ID " + id + " does not exist.");
        }
    }

    public ShopAdminDto getShopAdminByCellphone(final String cellphone) {
        var optionalShopAdmin = shopAdminRepository.findShopAdminByCellphone(cellphone);
        if (optionalShopAdmin.isPresent()) {
            return shopAdminMapper.toDto(optionalShopAdmin.get());
        } else {
            throw new EntityNotFoundException("Shop admin with cellphone " + cellphone + " does not exist.");
        }
    }

    public ShopAdminDto getShopAdminByFirebaseUid(final String firebaseUid) {
        var optionalShopAdmin = shopAdminRepository.findShopAdminByFirebaseUid(firebaseUid);
        if (optionalShopAdmin.isPresent()) {
            return shopAdminMapper.toDto(optionalShopAdmin.get());
        } else {
            throw new EntityNotFoundException("Shop admin with firebaseUID " + firebaseUid + " does not exist.");
        }
    }

}
