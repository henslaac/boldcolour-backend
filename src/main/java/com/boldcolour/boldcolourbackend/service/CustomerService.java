package com.boldcolour.boldcolourbackend.service;

import com.boldcolour.boldcolourbackend.model.dto.CustomerDto;
import com.boldcolour.boldcolourbackend.model.mapper.CustomerMapper;
import com.boldcolour.boldcolourbackend.model.repository.CustomerRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {
    private final CustomerMapper customerMapper;
    private final CustomerRepository customerRepository;

    public CustomerDto addCustomer(final CustomerDto customerDto) {
        var optionalCustomer = customerRepository.findCustomerByCellphone(customerDto.getCellphone());
        if (optionalCustomer.isEmpty()) {
            var customer = customerMapper.toEntity(customerDto);
            return customerMapper.toDto(customerRepository.save(customer));
        } else {
            throw new EntityExistsException("A customer with cellphone " + customerDto.getCellphone() + " already exists.");
        }
    }

    public void deleteCustomer(final Long id) {
        var optionalCustomer = customerRepository.findById(id);
        if (optionalCustomer.isPresent()) {
            customerRepository.delete(optionalCustomer.get());
        } else {
            throw new EntityNotFoundException("Customer with ID " + id + " does not exist.");
        }
    }

    public CustomerDto updateCustomer(final Long customerId, final CustomerDto customerDto) {
        var optionalCustomer = customerRepository.findById(customerId);
        if (optionalCustomer.isPresent()) {
            var customer = optionalCustomer.get();
            customer.setCellphone(customerDto.getCellphone());
            customer.setEmail(customerDto.getEmail());
            customer.setFcmToken(customerDto.getFcmToken());
            customer.setFirstName(customerDto.getFirstName());
            customer.setPhotoUrl(customerDto.getPhotoUrl());
            customer.setFirebaseUid(customerDto.getFirebaseUid());
            customer.setLastName(customerDto.getLastName());
            return customerMapper.toDto(customerRepository.save(customer));
        } else {
            throw new EntityNotFoundException("Customer with ID " + customerId + " does not exist.");
        }
    }

    public CustomerDto getCustomerByFirebaseUid(final String firebaseUid) {
        var optionalCustomer = customerRepository.findCustomerByFirebaseUid(firebaseUid);
        if (optionalCustomer.isPresent()) {
            return customerMapper.toDto(optionalCustomer.get());
        } else {
            throw new EntityNotFoundException("Customer with firebaseUid " + firebaseUid + " does not exist.");
        }
    }

    public CustomerDto getCustomerByCellphone(final String cellphone) {
        var optionalCustomer = customerRepository.findCustomerByCellphone(cellphone);
        if (optionalCustomer.isPresent()) {
            return customerMapper.toDto(optionalCustomer.get());
        } else {
            throw new EntityNotFoundException("Customer with cellphone " + cellphone + " does not exist.");
        }
    }
}
