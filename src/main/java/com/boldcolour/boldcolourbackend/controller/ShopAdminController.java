package com.boldcolour.boldcolourbackend.controller;

import com.boldcolour.boldcolourbackend.model.dto.ShopAdminDto;
import com.boldcolour.boldcolourbackend.service.ShopAdminService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/shop-admin")
public class ShopAdminController {
    private final ShopAdminService shopAdminService;

    @PostMapping
    @Operation(summary = "Adds a shop admin to the database.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Shop admin has been added successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ShopAdminDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.CREATED)
    public ShopAdminDto addShopAdmin(@RequestBody ShopAdminDto shopAdminDto) {
        log.info("Add shop admin {}", shopAdminDto.toString());
        return shopAdminService.addShopAdmin(shopAdminDto);
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a shop admin from the database based on ID.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Shop admin has been deleted successfully"
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public void deleteShopAdmin(@PathVariable Long id) {
        log.info("Delete Shop admin {}", id);
        shopAdminService.deleteShopAdmin(id);
    }

    @PostMapping("{id}")
    @Operation(summary = "Updates an existing shop admin.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Shop admin has been updated successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ShopAdminDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public ShopAdminDto updateShopAdmin(
            @PathVariable Long id,
            @RequestBody ShopAdminDto shopAdminDto
    ) {
        log.info("Update shop admin {}, {}", id, shopAdminDto.toString());
        return shopAdminService.updateShopAdmin(id, shopAdminDto);
    }

    @GetMapping("firebase-uid")
    @Operation(summary = "Gets a shop admin by firebase UID.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Shop admin has been fetched successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ShopAdminDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public ShopAdminDto getShopAdminByFirebaseUid(
            @RequestParam String firebaseUid
    ) {
        log.info("Getting shop admin by firebase UID {}", firebaseUid);
        return shopAdminService.getShopAdminByFirebaseUid(firebaseUid);
    }

    @GetMapping("cellphone")
    @Operation(summary = "Gets a shop admin by cellphone.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Shop admin has been fetched successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ShopAdminDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public ShopAdminDto getShopAdminByCellphone(
            @RequestParam String cellphone
    ) {
        log.info("Getting shop admin by cellphone {}", cellphone);
        return shopAdminService.getShopAdminByCellphone(cellphone);
    }
}
