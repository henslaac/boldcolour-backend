package com.boldcolour.boldcolourbackend.controller;


import com.boldcolour.boldcolourbackend.model.dto.CustomerDto;
import com.boldcolour.boldcolourbackend.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping
    @Operation(summary = "Adds a customer to the database. ")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Customer has been added successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = CustomerDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.CREATED)
    public CustomerDto addCustomer(@RequestBody CustomerDto customerDto) {
        log.info("Add customer {}", customerDto.toString());
        return customerService.addCustomer(customerDto);
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a customer from the database based on ID ")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Customer has been deleted successfully"
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public void deleteCustomer(@PathVariable Long id) {
        log.info("Delete customer {}", id);
        customerService.deleteCustomer(id);
    }

    @PostMapping("{id}")
    @Operation(summary = "Updates an existing customer.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Customer has been updated successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = CustomerDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public CustomerDto updateCustomer(
            @PathVariable Long id,
            @RequestBody CustomerDto customerDto
    ) {
        log.info("Update customer {}, {}", id, customerDto.toString());
        return customerService.updateCustomer(id, customerDto);
    }

    @GetMapping("firebase-uid")
    @Operation(summary = "Gets a customer by firebase UID.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Customer has been fetched successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = CustomerDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public CustomerDto getCustomerByFirebaseUid(
            @RequestParam String firebaseUid
    ) {
        log.info("Getting customer by firebase UID {}", firebaseUid);
        return customerService.getCustomerByFirebaseUid(firebaseUid);
    }

    @GetMapping("cellphone")
    @Operation(summary = "Gets a customer by cellphone.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Customer has been fetched successfully",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = CustomerDto.class)
                            )
                    }
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public CustomerDto getCustomerByCellphone(
            @RequestParam String cellphone
    ) {
        log.info("Getting customer by cellphone {}", cellphone);
        return customerService.getCustomerByCellphone(cellphone);
    }

}
