package com.boldcolour.boldcolourbackend.controller.advice;

import com.boldcolour.boldcolourbackend.model.dto.BaseErrorResponse;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalControllerExceptionHandler {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<BaseErrorResponse> handleEntityNotFound(RuntimeException e) {
        log.warn(e.getMessage());
        return new ResponseEntity<>(new BaseErrorResponse(404, e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler({EntityExistsException.class})
    public ResponseEntity<BaseErrorResponse> handleEntityExists(RuntimeException e) {
        log.warn(e.getMessage());
        return new ResponseEntity<>(new BaseErrorResponse(409, e.getMessage()), HttpStatus.CONFLICT);
    }
}
