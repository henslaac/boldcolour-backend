package com.boldcolour.boldcolourbackend.model.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ShopAdminDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String cellphone;
    private String email;
    private String firebaseUid;
    private String fcmToken;
    private String photoUrl;
}
