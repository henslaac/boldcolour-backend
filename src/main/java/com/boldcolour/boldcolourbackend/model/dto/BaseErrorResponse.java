package com.boldcolour.boldcolourbackend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseErrorResponse {
    private int status;
    private String message;
}
