package com.boldcolour.boldcolourbackend.model.repository;

import com.boldcolour.boldcolourbackend.model.entity.ShopAdmin;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ShopAdminRepository extends CrudRepository<ShopAdmin, Long> {
    Optional<ShopAdmin> findShopAdminByCellphone(String cellphone);
    Optional<ShopAdmin> findShopAdminByFirebaseUid(String firebaseUid);
}
