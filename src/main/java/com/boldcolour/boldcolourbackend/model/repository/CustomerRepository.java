package com.boldcolour.boldcolourbackend.model.repository;

import com.boldcolour.boldcolourbackend.model.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
    Optional<Customer> findCustomerByCellphone(String cellphone);
    Optional<Customer> findCustomerByFirebaseUid(String firebaseUid);
}
