package com.boldcolour.boldcolourbackend.model.mapper;

import com.boldcolour.boldcolourbackend.model.dto.ShopAdminDto;
import com.boldcolour.boldcolourbackend.model.entity.ShopAdmin;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ShopAdminMapper extends EntityMapper<ShopAdmin, ShopAdminDto> {
}
