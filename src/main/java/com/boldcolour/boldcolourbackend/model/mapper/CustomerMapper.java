package com.boldcolour.boldcolourbackend.model.mapper;

import com.boldcolour.boldcolourbackend.model.dto.CustomerDto;
import com.boldcolour.boldcolourbackend.model.entity.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<Customer, CustomerDto> {
}
