package com.boldcolour.boldcolourbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoldcolourBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoldcolourBackendApplication.class, args);
    }

}
