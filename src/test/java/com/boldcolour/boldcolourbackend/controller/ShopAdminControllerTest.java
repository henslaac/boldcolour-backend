package com.boldcolour.boldcolourbackend.controller;

import com.boldcolour.boldcolourbackend.model.dto.ShopAdminDto;
import com.boldcolour.boldcolourbackend.service.ShopAdminService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ShopAdminController.class)
public class ShopAdminControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ShopAdminService shopAdminService;
    private ShopAdminDto shopAdminDto;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setup() {
        shopAdminDto = ShopAdminDto.builder()
                .id(1L)
                .cellphone("0800111222")
                .email("test@email.com")
                .fcmToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
                .firebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
                .firstName("John")
                .lastName("Doe")
                .photoUrl("https://firebasestorage.googleapis.com/v0/b/your-firebase-project-id.appspot.com/o/images%2Fsample-photo.jpg?alt=media")
                .build();
    }

    @Test
    @DisplayName("Test add shop admin success")
    void addShopAdminTest_success() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/shop-admin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(shopAdminDto))
        ).andExpect(status().isCreated());
    }

    @Test
    @DisplayName("Test add shop admin failed")
    void addShopAdminTest_failed() throws Exception {
        when(shopAdminService.addShopAdmin(any())).thenThrow(new EntityExistsException("A shop admin with cellphone" + shopAdminDto.getCellphone() + " already exists."));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/shop-admin")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(shopAdminDto))
        ).andExpect(status().isConflict());
    }

    @Test
    @DisplayName("Test delete shop admin")
    void deleteShopAdminTest_failed() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/shop-admin/{id}", 1L)
        ).andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test update shop admin failed")
    void updateShopAdminTest_failed() throws Exception {
        when(shopAdminService.updateShopAdmin(anyLong(), any())).thenThrow(new EntityNotFoundException("Shop admin with ID " + 1 + " does not exist."));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/shop-admin/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(shopAdminDto))
        ).andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Test update shop admin success")
    void updateShopAdminTest_success() throws Exception {
        when(shopAdminService.updateShopAdmin(anyLong(), any())).thenReturn(shopAdminDto);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/shop-admin/{id}", 1L)
                        .content(objectMapper.writeValueAsString(shopAdminDto))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test get shop admin by firebase UID failed")
    void getShopAdminByFirebaseUidTest_failed() throws Exception {
        when(shopAdminService.getShopAdminByFirebaseUid(anyString())).thenThrow(new EntityNotFoundException("Shop admin with firebase UID 5pP3c8EXXXXXXXXXXXXXXXXXXXXXX does not exist."));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/shop-admin/firebase-uid")
                        .param("firebaseUid", "5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
        ).andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Test get show admin by firebase UID success")
    void getShopAdminByFirebaseUidTest_success() throws Exception {
        when(shopAdminService.getShopAdminByFirebaseUid(anyString())).thenReturn(shopAdminDto);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/shop-admin/firebase-uid")
                        .param("firebaseUid", "5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
        ).andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test get shop admin by cellphone failed")
    void getShopAdminByCellphoneTest_failed() throws Exception {
        when(shopAdminService.getShopAdminByCellphone(anyString())).thenThrow(new EntityNotFoundException("Shop admin with cellphone 0800111222 does not exist."));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/shop-admin/cellphone")
                        .param("cellphone", "0800111222")
        ).andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Test get shop admin by cellphone success")
    void getShopAdminByCellphoneTest_success() throws Exception {
        when(shopAdminService.getShopAdminByCellphone(anyString())).thenReturn(shopAdminDto);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/shop-admin/cellphone")
                        .param("cellphone", "0800111222")
        ).andExpect(status().isOk());
    }
}
