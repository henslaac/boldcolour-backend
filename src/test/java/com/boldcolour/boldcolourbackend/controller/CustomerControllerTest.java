package com.boldcolour.boldcolourbackend.controller;

import com.boldcolour.boldcolourbackend.model.dto.CustomerDto;
import com.boldcolour.boldcolourbackend.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CustomerController.class)
public class CustomerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomerService customerService;
    private CustomerDto customerDto;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setup() {
        customerDto = CustomerDto.builder()
                .id(1L)
                .cellphone("0800111222")
                .email("test@email.com")
                .fcmToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
                .firebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
                .firstName("John")
                .lastName("Doe")
                .photoUrl("https://firebasestorage.googleapis.com/v0/b/your-firebase-project-id.appspot.com/o/images%2Fsample-photo.jpg?alt=media")
                .build();
    }

    @Test
    @DisplayName("Test add customer success")
    void addCustomerTest_success() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/customer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerDto))
        ).andExpect(status().isCreated());
    }

    @Test
    @DisplayName("Test add customer failed")
    void addCustomerTest_failed() throws Exception {
        when(customerService.addCustomer(any())).thenThrow(new EntityExistsException("A customer with cellphone" + customerDto.getCellphone() + " already exists."));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/customer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerDto))
        ).andExpect(status().isConflict());
    }

    @Test
    @DisplayName("Test delete customer")
    void deleteCustomer_failed() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete("/customer/{id}", 1L)
        ).andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test update customer failed")
    void updateCustomer_failed() throws Exception {
        when(customerService.updateCustomer(anyLong(), any())).thenThrow(new EntityNotFoundException("Customer with ID " + 1 + " does not exist."));
        mockMvc.perform(
                MockMvcRequestBuilders.post("/customer/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(customerDto))
        ).andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Test update customer success")
    void updateCustomer_success() throws Exception {
        when(customerService.updateCustomer(anyLong(), any())).thenReturn(customerDto);
        mockMvc.perform(
                MockMvcRequestBuilders.post("/customer/{id}", 1L)
                        .content(objectMapper.writeValueAsString(customerDto))
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test get customer by firebase UID failed")
    void getCustomerByFirebaseUid_failed() throws Exception {
        when(customerService.getCustomerByFirebaseUid(anyString())).thenThrow(new EntityNotFoundException("Customer with firebase UID 5pP3c8EXXXXXXXXXXXXXXXXXXXXXX does not exist."));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/customer/firebase-uid")
                        .param("firebaseUid", "5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
        ).andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Test get customer by firebase UID success")
    void getCustomerByFirebaseUid_success() throws Exception {
        when(customerService.getCustomerByFirebaseUid(anyString())).thenReturn(customerDto);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/customer/firebase-uid")
                        .param("firebaseUid", "5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
        ).andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test get customer by cellphone failed")
    void getCustomerByCellphone_failed() throws Exception {
        when(customerService.getCustomerByCellphone(anyString())).thenThrow(new EntityNotFoundException("Customer with cellphone 0800111222 does not exist."));
        mockMvc.perform(
                MockMvcRequestBuilders.get("/customer/cellphone")
                        .param("cellphone", "0800111222")
        ).andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Test get customer by cellphone success")
    void getCustomerByCellphone_success() throws Exception {
        when(customerService.getCustomerByCellphone(anyString())).thenReturn(customerDto);
        mockMvc.perform(
                MockMvcRequestBuilders.get("/customer/cellphone")
                        .param("cellphone", "0800111222")
        ).andExpect(status().isOk());
    }
}
