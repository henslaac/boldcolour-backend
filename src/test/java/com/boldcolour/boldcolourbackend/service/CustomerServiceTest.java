package com.boldcolour.boldcolourbackend.service;

import com.boldcolour.boldcolourbackend.model.dto.CustomerDto;
import com.boldcolour.boldcolourbackend.model.entity.Customer;
import com.boldcolour.boldcolourbackend.model.mapper.CustomerMapper;
import com.boldcolour.boldcolourbackend.model.repository.CustomerRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = CustomerService.class)
public class CustomerServiceTest {
    @Autowired
    private CustomerService customerService;
    @MockBean
    private CustomerMapper customerMapper;
    @MockBean
    private CustomerRepository customerRepository;

    private CustomerDto customerDto;
    private Customer customer;

    @BeforeEach
    void setUp() {
        customerDto = CustomerDto.builder()
                .id(1L)
                .cellphone("0800111222")
                .email("test@email.com")
                .fcmToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
                .firebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
                .firstName("John")
                .lastName("Doe")
                .photoUrl("https://firebasestorage.googleapis.com/v0/b/your-firebase-project-id.appspot.com/o/images%2Fsample-photo.jpg?alt=media")
                .build();

        customer = new Customer(
                1L, "John", "Doe", "0800111222", "test@email.com",
                "5pP3c8EXXXXXXXXXXXXXXXXXXXXXX",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
                "https://firebasestorage.googleapis.com/v0/b/your-firebase-project-id.appspot.com/o/images%2Fsample-photo.jpg?alt=media"
        );
    }

    @Test
    @DisplayName("Test add customer - not exist")
    void addCustomer_notExist() {
        when(customerRepository.findCustomerByCellphone(anyString())).thenReturn(Optional.empty());
        when(customerMapper.toEntity(any())).thenReturn(customer);
        when(customerMapper.toDto(any())).thenReturn(customerDto);
        var result = customerService.addCustomer(customerDto);
        assert result == customerDto;
        verify(customerRepository, times(1)).findCustomerByCellphone(customerDto.getCellphone());
        verify(customerRepository, times(1)).save(customer);
    }

    @Test
    @DisplayName("Test add customer - exists")
    void addCustomer_exists() {
        when(customerRepository.findCustomerByCellphone(anyString())).thenReturn(Optional.of(customer));
        assertThrows(EntityExistsException.class, () -> customerService.addCustomer(customerDto));
        verify(customerRepository, times(1)).findCustomerByCellphone(customerDto.getCellphone());
        verify(customerRepository, times(0)).save(customer);
    }

    @Test
    @DisplayName("Test delete customer - not exist")
    void deleteCustomer_notExist() {
        when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> customerService.deleteCustomer(1L));
        verify(customerRepository, times(1)).findById(1L);
    }

    @Test
    @DisplayName("Test delete customer - exists")
    void deleteCustomer_exist() {
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(customer));
        customerService.deleteCustomer(1L);
        verify(customerRepository, times(1)).findById(1L);
        verify(customerRepository, times(1)).delete(customer);
    }

    @Test
    @DisplayName("Test update customer - not exist")
    void updateCustomerTest_notExist() {
        when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> customerService.updateCustomer(1L, customerDto));
        verify(customerRepository, times(1)).findById(1L);
    }

    @Test
    @DisplayName("Test update customer - exists")
    void updateCustomerTest_exists() {
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(customer));
        when(customerMapper.toEntity(any())).thenReturn(customer);
        when(customerMapper.toDto(any())).thenReturn(customerDto);
        var result = customerService.updateCustomer(1L, customerDto);
        assert result == customerDto;
        verify(customerRepository, times(1)).findById(1L);
        verify(customerRepository, times(1)).save(customer);
    }

    @Test
    @DisplayName("Test get customer by firebaseUid - not exist")
    void getCustomerByFirebaseUid_failed() {
        when(customerRepository.findCustomerByFirebaseUid(anyString())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> customerService.getCustomerByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX"));
        verify(customerRepository, times(1)).findCustomerByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX");
    }

    @Test
    @DisplayName("Test get customer by firebaseUid - exists")
    void getCustomerByFirebaseUid_success() {
        when(customerRepository.findCustomerByFirebaseUid(anyString())).thenReturn(Optional.of(customer));
        when(customerMapper.toDto(any())).thenReturn(customerDto);
        var result = customerService.getCustomerByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX");
        assert result == customerDto;
        verify(customerRepository, times(1)).findCustomerByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX");
        verify(customerMapper, times(1)).toDto(customer);
    }

    @Test
    @DisplayName("Test get customer by cellphone - not exist")
    void getCustomerByCellphone_failed() {
        when(customerRepository.findCustomerByCellphone(anyString())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> customerService.getCustomerByCellphone("0800111222"));
        verify(customerRepository, times(1)).findCustomerByCellphone("0800111222");
    }

    @Test
    @DisplayName("Test get customer by cellphone - exists")
    void getCustomerByCellphone_success() {
        when(customerRepository.findCustomerByCellphone(anyString())).thenReturn(Optional.of(customer));
        when(customerMapper.toDto(any())).thenReturn(customerDto);
        var result = customerService.getCustomerByCellphone("0800111222");
        assert result == customerDto;
        verify(customerRepository, times(1)).findCustomerByCellphone("0800111222");
        verify(customerMapper, times(1)).toDto(customer);
    }

}
