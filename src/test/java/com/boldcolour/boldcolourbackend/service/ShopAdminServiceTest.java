package com.boldcolour.boldcolourbackend.service;

import com.boldcolour.boldcolourbackend.model.dto.ShopAdminDto;
import com.boldcolour.boldcolourbackend.model.entity.ShopAdmin;
import com.boldcolour.boldcolourbackend.model.mapper.ShopAdminMapper;
import com.boldcolour.boldcolourbackend.model.repository.ShopAdminRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest(classes = ShopAdminService.class)
public class ShopAdminServiceTest {
    @Autowired
    private ShopAdminService shopAdminService;
    @MockBean
    private ShopAdminMapper shopAdminMapper;
    @MockBean
    private ShopAdminRepository shopAdminRepository;

    private ShopAdmin shopAdmin;
    private ShopAdminDto shopAdminDto;

    @BeforeEach
    void setUp() {
        shopAdmin = new ShopAdmin(
                1L, "John", "Doe", "0800111222", "test@email.com",
                "5pP3c8EXXXXXXXXXXXXXXXXXXXXXX",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
                "https://firebasestorage.googleapis.com/v0/b/your-firebase-project-id.appspot.com/o/images%2Fsample-photo.jpg?alt=media"
        );

        shopAdminDto = ShopAdminDto.builder()
                .id(1L)
                .cellphone("0800111222")
                .email("test@email.com")
                .fcmToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
                .firebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX")
                .firstName("John")
                .lastName("Doe")
                .photoUrl("https://firebasestorage.googleapis.com/v0/b/your-firebase-project-id.appspot.com/o/images%2Fsample-photo.jpg?alt=media")
                .build();
    }

    @Test
    @DisplayName("Test add shop admin - not exist")
    void addShopAdminTest_notExist() {
        when(shopAdminRepository.findShopAdminByCellphone(anyString())).thenReturn(Optional.empty());
        when(shopAdminMapper.toEntity(any())).thenReturn(shopAdmin);
        when(shopAdminMapper.toDto(any())).thenReturn(shopAdminDto);
        var result = shopAdminService.addShopAdmin(shopAdminDto);
        assert result == shopAdminDto;
        verify(shopAdminRepository, times(1)).findShopAdminByCellphone(shopAdminDto.getCellphone());
        verify(shopAdminRepository, times(1)).save(shopAdmin);
    }

    @Test
    @DisplayName("Test add shop admin - exists")
    void addSopAdminTest_exists() {
        when(shopAdminRepository.findShopAdminByCellphone(anyString())).thenReturn(Optional.of(shopAdmin));
        assertThrows(EntityExistsException.class, () -> shopAdminService.addShopAdmin(shopAdminDto));
        verify(shopAdminRepository, times(1)).findShopAdminByCellphone(shopAdminDto.getCellphone());
        verify(shopAdminRepository, times(0)).save(shopAdmin);
    }

    @Test
    @DisplayName("Test delete shop admin - not exist")
    void deleteShopAdminTest_notExist() {
        when(shopAdminRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> shopAdminService.deleteShopAdmin(1L));
        verify(shopAdminRepository, times(1)).findById(1L);
        verify(shopAdminRepository, times(0)).delete(shopAdmin);
    }

    @Test
    @DisplayName("Test delete shop admin - exists")
    void deleteShopAdminTest_exist() {
        when(shopAdminRepository.findById(anyLong())).thenReturn(Optional.of(shopAdmin));
        shopAdminService.deleteShopAdmin(1L);
        verify(shopAdminRepository, times(1)).findById(1L);
        verify(shopAdminRepository, times(1)).delete(shopAdmin);
    }

    @Test
    @DisplayName("Test update shop admin - not exist")
    void updateShopAdminTest_notExist() {
        when(shopAdminRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> shopAdminService.updateShopAdmin(1L, shopAdminDto));
        verify(shopAdminRepository, times(1)).findById(1L);
    }

    @Test
    @DisplayName("Test update shop admin - exists")
    void updateShopAdminTest_exists() {
        when(shopAdminRepository.findById(anyLong())).thenReturn(Optional.of(shopAdmin));
        when(shopAdminMapper.toEntity(any())).thenReturn(shopAdmin);
        when(shopAdminMapper.toDto(any())).thenReturn(shopAdminDto);
        var result = shopAdminService.updateShopAdmin(1L, shopAdminDto);
        assert result == shopAdminDto;
        verify(shopAdminRepository, times(1)).findById(1L);
        verify(shopAdminRepository, times(1)).save(shopAdmin);
    }

    @Test
    @DisplayName("Test get shop admin by firebaseUid - not exist")
    void getShopAdminByFirebaseUidTest_failed() {
        when(shopAdminRepository.findShopAdminByFirebaseUid(anyString())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> shopAdminService.getShopAdminByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX"));
        verify(shopAdminRepository, times(1)).findShopAdminByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX");
    }

    @Test
    @DisplayName("Test get shop admin by firebaseUid - exists")
    void getShopAdminByFirebaseUidTest_success() {
        when(shopAdminRepository.findShopAdminByFirebaseUid(anyString())).thenReturn(Optional.of(shopAdmin));
        when(shopAdminMapper.toDto(any())).thenReturn(shopAdminDto);
        var result = shopAdminService.getShopAdminByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX");
        assert result == shopAdminDto;
        verify(shopAdminRepository, times(1)).findShopAdminByFirebaseUid("5pP3c8EXXXXXXXXXXXXXXXXXXXXXX");
        verify(shopAdminMapper, times(1)).toDto(shopAdmin);
    }

    @Test
    @DisplayName("Test get shop admin by cellphone - not exist")
    void getShopAdminByCellphoneTest_failed() {
        when(shopAdminRepository.findShopAdminByCellphone(anyString())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> shopAdminService.getShopAdminByCellphone("0800111222"));
        verify(shopAdminRepository, times(1)).findShopAdminByCellphone("0800111222");
    }

    @Test
    @DisplayName("Test get shop admin by cellphone - exists")
    void getShopAdminByCellphoneTest_success() {
        when(shopAdminRepository.findShopAdminByCellphone(anyString())).thenReturn(Optional.of(shopAdmin));
        when(shopAdminMapper.toDto(any())).thenReturn(shopAdminDto);
        var result = shopAdminService.getShopAdminByCellphone("0800111222");
        assert result == shopAdminDto;
        verify(shopAdminRepository, times(1)).findShopAdminByCellphone("0800111222");
        verify(shopAdminMapper, times(1)).toDto(shopAdmin);
    }
}
