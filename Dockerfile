FROM azul/zulu-openjdk:21 as builder
WORKDIR /app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN chmod +x ./mvnw && ./mvnw clean package -DskipTests

FROM azul/zulu-openjdk-alpine:21-jre
WORKDIR /app
RUN ls -R /app
COPY --from=builder /app/target/*.jar app.jar

EXPOSE 8080

CMD ["java", "-Dspring.profiles.active=prod", "-jar", "app.jar"]